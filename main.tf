provider "aws" {}

terraform {

  backend "s3" {
    key    = "app.tfstate"
    region = "eu-west-1"
  }

}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "devops-bootcamp-202012-${var.alumno_slug}"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}

resource "aws_elb" "ingress" {
  name = "${var.alumno_slug}"

  subnets         = ["${data.terraform_remote_state.vpc.subnet_id}"]
  security_groups = ["${aws_security_group.ingress.id}"]
  instances       = ["${aws_instance.web.*.id}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
}

resource "aws_security_group" "ingress" {
  name        = "${var.alumno_slug}_ingress"
  description = "Used in the terraform"
  vpc_id      = "${data.terraform_remote_state.vpc.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_ami" "this" {
  most_recent = true
  filter {
    name   = "name"
    values = ["${var.alumno_slug}*"]
  }
  owners = ["268229342313"] # Geekshubs
}

resource "aws_security_group" "main" {
  name        = "${var.alumno_slug}"
  description = "Used in the terraform"
  vpc_id      = "${data.terraform_remote_state.vpc.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {

  count = "${var.numero_de_instancias}"

  ami           = "${data.aws_ami.this.id}"
  instance_type = "t3.micro"

  associate_public_ip_address = true
  vpc_security_group_ids = ["${aws_security_group.main.id}"]
  subnet_id = "${data.terraform_remote_state.vpc.subnet_id}"

  tags = {
    Name = "${var.alumno_slug}"
  }

  # key_name = "${aws_key_pair.auth.key_name}"

}

# resource "aws_key_pair" "auth" {
#   key_name   = "${var.alumno}"
#   public_key = "${tls_private_key.example.public_key_openssh}"
# }
#
# resource "tls_private_key" "example" {
#   algorithm = "RSA"
#   rsa_bits  = 4096
# }
#
# output "id_rsa" {
#   value = "${tls_private_key.example.private_key_pem}"
# }
