output "public_address" {
  value = "${aws_elb.ingress.dns_name}"
}

output "public_ip" {
  value = ["${aws_instance.web.*.public_ip}"]
}
